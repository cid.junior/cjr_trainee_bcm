import React from 'react';


class Counter extends React.Component{
    constructor(props){
      super(props)
      this.state = {
        contador: 0,
      };
    }
    somar1 = () =>  {
      this.setState({
        contador: this.state.contador + 1
      }) 
    }
    subtrair1 = () =>  {
      this.setState({
        contador: this.state.contador - 1
      }) 
    }
    
    render() {
      return (
        <>
        <div>counter: {this.state.contador}</div>
        <button onClick={this.somar1} > + </button>
        <button onClick={this.subtrair1}> - </button>
        </>
      );
    }
}
            
  export default Counter;