import React from 'react';
import './header.css'

const Header = () => {
        return (
                <header>
                <h1>Organizador de Tarefas</h1>
                <ul>
                        <li><a href="inicial">Página Inicial</a></li>
                        <li><a href="sobre_nos">Sobre Nós</a></li>
                        <li><a href="fale_conosco">Fale Conosco</a></li>
                </ul>   
                </header>
        );
}
export default Header;