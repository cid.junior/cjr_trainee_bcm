import React from 'react';
import './App.css';
import Body from './Components/body'
import Header from './Components/header'
import Footer from './Components/footer'


function App() {
  return (
    <>  
      <Header/>
      <Body />
      <Footer />
    </>
  );
}

export default App;
